# polyanya-triangulations-packed

Triangulations generated for use in our Polyanya implementation, converted
from `movingai-benchmarks`. Used in our paper
[Compromise-free Pathfinding on a Navigation Mesh](http://www.ijcai.org/proceedings/2017/0070.pdf).

These triangulations are packed - unpack these using the `meshunpacker` utility
in the `utils` directory of the
[C++ implementation](https://bitbucket.org/mlcui1/polyanya).

If you somehow stumble across this repository without access to the C++
implementation, you can unpack a mesh by:

1. Read the "`pack`" header.
2. Output "`mesh`", and a new line.
3. For every triplet of bytes after the header, convert it into an integer. 
   For example, 0x000014 means decimal 20.
   Output this integer, then a new line.

As an example, the first few lines of the unpacked `dao/arena.mesh.packed` is:
```
mesh
2
112
120
2
2
2
-1
4
1
```